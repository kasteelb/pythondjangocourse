# Deze week is er Glueview data beschikbaar!
# Dat lijkt een beetje op BlueView data; dus dat is leuk om mee te oefenen :)
import json
import pprint  # Om de dictionaries wat mooier te printen


with open('Week4/glueview_data.json') as file_path:
    glueview_data = json.load(file_path)

pprint.pprint(glueview_data[0])


# De eerste opdracht is als volgt:
# 1. Maak een nieuwe Git branch aan met jouw naam er in. Bijvoorbeeld 'branch_bram'
# 2. Zorg dat deze branch actief is, dat je op die branch gaat werken. Hoe weet je op welke branch je momenteel zit?
# 3. Schrijf code die de personen uit de eerste registratie print (check ook eerst ff of bovenstaande werkt)
# 4. Schrijf een persoonlijk complimentje naar je teamgenoten
# 5. Commit je code, push je code.
# 6. Check online of je branch is aangemaakt en of je code er in staat!
