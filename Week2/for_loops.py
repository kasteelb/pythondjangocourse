# Besproken in week 2
import json


def lost_without_a_map(list_numbers, solution_choice=1):
    '''
    input: list_numbers, a list of numbers
    output: a list with eath element doubled: [1,2,3] --> [2,4,6]
        (https://www.codewars.com/kata/57f781872e3d8ca2a000007e)
    '''

    if solution_choice == 1:  # Ugly loop  --> You don't need counter, we use append
        output = []
        for counter in range(0, len(list_numbers)):
            output.append(2*list_numbers[counter])
        return output

    if solution_choice == 2:  # Better loop --> Directly loop over all the items. Ugly: we still use append
        output = []
        for number in list_numbers:
            output.append(2*number)
        return output

    if solution_choice == 3:  # List comprehension --> Directly loop over all the times, no append
        return [2*number for number in list_numbers]

    if solution_choice == 4:  # Map function --> Comparable with list-comprehension
        def times_two(num):
            return num * 2
        return list(map(times_two, list_numbers))

    if solution_choice == 5:  # Map function --> Comparable with list-comprehension
        return list(map(lambda x: 2*x, list_numbers))


# Een oplossing voor deze gebruikte een enumerate: wat is deze ding?!?!
def mumbling(s, solution_choice=1):
    '''
    Mubling (week 1)
    https://www.codewars.com/kata/5667e8f4e3f572a8f2000039
    accum("abcd") -> "A-Bb-Ccc-Dddd"
    accum("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
    accum("cwAt") -> "C-Ww-Aaa-Tttt"
    '''

    if solution_choice == 1:
        return '-'.join(x.upper() + x.lower() * count for count, x in enumerate(s))

    if solution_choice == 2:
        output = []
        for count, x in enumerate(s):  # 'test' --> (0, 't'), (1, 'e'), (2, 's'), (3, 't')
            print(count)
            print(x)
            mumble = x.upper() + x.lower() * count
            print(mumble)
            output.append(mumble)
        return '-'.join(output)


# Todo: laten zien hoe importeren werkt met een shortkey: alt + enter
with open('large_json.json', 'r') as f:
    json_data = json.load(f)

print(json_data)
