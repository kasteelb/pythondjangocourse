import re

# Let op: deze string begint met de letter f; dan kunnen we later highlights gebruiken :)
text = f"Verdachte Bram is onlangs aangehouden ivm overtreden avondklok." \
        "Bij controle bleek hij wat wiet en een blikje coke op zak te hebben. Antecedenten drugshandel. " \
        "Zijn vader heeft een wietplantage en drugs verslaving gehad; ook een interessant feit."


''' Voorbeelden re.findall'''
woord = 'wiet'
pattern_wiet = re.compile(woord)

result_wiet = re.findall(pattern_wiet, text)
print(result_wiet)

query = 'wiet[a-zA-Z]*'
pattern_wiet_star = re.compile(query)
result_wiet_star = re.findall(pattern_wiet_star, text)
print(result_wiet_star)

''' Voorbeeld re.sub'''
query_drugs = 'drugs[a-zA-Z]*'
pattern_drugs_star = re.compile(query_drugs)
result_drugs_star = re.sub(pattern_drugs_star, '[WEGGEHAALD]', text)
print(result_drugs_star)


''' Vette query met highlights'''
query_parts = ['wiet[a-zA-Z]*', 'drugs[a-zA-Z]*', 'coke']
patterns = [re.compile(pattern) for pattern in query_parts]


def highlighter(matchobj: str):
    start = '\033[93m'
    end = '\033[0m'
    return f'{start}' + matchobj.group(0) + f'{end}'


highlighted_text = text
for pattern in patterns:
    highlighted_text = re.sub(pattern, highlighter, highlighted_text)

print(highlighted_text)