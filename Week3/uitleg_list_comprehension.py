lijst = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

'''Hoe bouw ik een list comprehension'''

# Opdracht: alleen de even getallen terug te geven

# Stap 1: de lijst opnieuw bouwen
result = [item for item in lijst]
print(result)

# Stap 2: functionaliteit toevoegen. Meestal is dat een 'if' aan het eind!
result2 = [item for item in lijst if item % 2 == 0]
print(result2)


# Andere mogelijkheid: een functie toepassen
# 1: [item for item in lijst]
def even(getal):  # (geeft True of False terug)
    return getal % 2 == 0


#2
result3 = [even(item) for item in lijst]
print(result3)
