# ALlemaal functies die bepalen hoeveel agenten we gaan inzetten

class InzetBepaling():
    # Misdaadcijfers ~ [4, 10, 2]
    def __init__(self, misdaadcijfers, max_agenten, **kwargs):
        self.misdaadcijfers = misdaadcijfers
        self.max = max_agenten

        if kwargs.get('weer') is not None:
            self.weer = kwargs['weer']
        else:
            self.weer = None

    def agenten_op_straat(self):
        return min(sum(self.misdaadcijfers)*2, self.max)

    def agenten_op_kantoor(self):
        return self.agenten_op_straat()*3


inzet_bepaling_rotterdam = InzetBepaling([2, 12, 33], 80, weer=True)
inzet_bepaling_rotterdam.weer = False

print(inzet_bepaling_rotterdam.weer)
print(inzet_bepaling_rotterdam.agenten_op_straat())


class InzetBepaling2021(InzetBepaling):

    def agenten_op_kantoor(self):
        return self.agenten_op_straat()*4


inzet_bepaling_rotterdam = InzetBepaling2021([2, 12, 33], 80, weer=True)
print(inzet_bepaling_rotterdam.agenten_op_kantoor())


# Opmerking: met _ voor een functie of variabele in je klasse kun je 'doen alsof' hij private is.
# Niets verbiedt je om hem te overschrijven of te gebruiken buiten te klasse, maar het betekent: stay away!
# PyCharm, of andere editors, geven wel een waarschuwing als je zo'n 'private' variabele probeert te setten.




