# Comparison operators, if statements
# Dingen zoals: if (a == 10):, b > 10, z < 9, woord[0]=='b'

# Todo: 1
# Maak een functie, met behulp van een for-loop die een lijst teruggeeft, met alleen de waardes die groter zijn dan 10.
# De waardes stellen het aantallen diefstallen voor in de eenheden
diefstallen = [11, 3, 5, 14, 3, 121, 18, 2, 39, 55, 8]


def groter_dan_tien(lijst):
    pass


assert (groter_dan_tien(diefstallen) == [11, 14, 121, 18, 39, 55])


# Todo: 2
# Probeer bovenstaande functie te schrijven, maar dan met een list-comprehension
def groter_dan_tien_list_comprehension(lijst):
    pass


assert (groter_dan_tien_list_comprehension(diefstallen) == [11, 14, 121, 18, 39, 55])


# Todo: 3
# Schrijf een functie die de waardes in een lijst teruggeeft als ze groter zijn dan 10, maar kleiner dan 45
def in_juiste_range(lijst):
    pass


assert (in_juiste_range(diefstallen) == [11, 14, 18, 39])


# Todo: 4
# Schrijf een functie die de registratienummers teruggeeft als ze uit PL0600 of PL1100 komen.
def mijn_pl(lijst):
    pass


assert (mijn_pl(['PL0600_REGISTRATIE_12231234', 'PL0800_REGISTRATIE_123214', 'PL1100_REGISTRATIE_12231234']) ==
        ['PL0600_REGISTRATIE_12231234', 'PL1100_REGISTRATIE_12231234'])
assert (mijn_pl(['PL0700_REGISTRATIE_12231234', 'PL1000_REGISTRATIE_123214', 'PL9999_REGISTRATIE_12231234']) ==
        [])
assert (mijn_pl(['PL0700_REGISTRATIE_12231234', 'PL1100_REGISTRATIE_123214', 'PL9999_REGISTRATIE_12231234']) ==
    ['PL1100_REGISTRATIE_123214'])


# Todo: 5
# Schrijf een functie die de registratienummers teruggeeft als ze uit PL0600 of PL1100 komen,
# MAAR DAN MET EEN LIST COMPREHENSION
def mijn_pl_2(lijst):
    pass


assert (mijn_pl_2(['PL0600_REGISTRATIE_12231234', 'PL0800_REGISTRATIE_123214', 'PL1100_REGISTRATIE_12231234']) ==
        ['PL0600_REGISTRATIE_12231234', 'PL1100_REGISTRATIE_12231234'])
assert (mijn_pl_2(['PL0700_REGISTRATIE_12231234', 'PL1000_REGISTRATIE_123214', 'PL9999_REGISTRATIE_12231234']) ==
        [])
assert (mijn_pl_2(['PL0700_REGISTRATIE_12231234', 'PL1100_REGISTRATIE_123214', 'PL9999_REGISTRATIE_12231234']) ==
    ['PL1100_REGISTRATIE_123214'])


# Todo: 6 (uitdaging!?)
# De input ziet er deze keer anders uit: een dictionary!
# Geef de namen van de eenheden terug, in een lijst, waar het misdaadcijfer boven de 25 uit komt
# Moeilijk, want hoe doe je nu een for-loop over een dictionary!?
misdaadcijfer = {
    'Midden-Nederland': 13,
    'Oost-Nederland': 28,
    'Zebra': 99,
    'Noord-Nederland': 7,
    'Amsterdam': 13,
    'Noord-Holland': 56,
    'Den Haag': 14,
    'Rotterdam': 27,
    'Limburg': 33,
    'Oost-Brabant': 12,
    'Landelijke eenheid': 124
}


def groot_misdaadcijfer(jaarcijfers):
    return []


assert(set(groot_misdaadcijfer(misdaadcijfer)) ==
       set(['Oost-Nederland', 'Zebra', 'Noord-Holland', 'Rotterdam', 'Limburg', 'Landelijke eenheid']))


# Todo 7 (uitdaging?!)
# Doe hetzelfde als hierboven, maar dan met een list-comprehension
def groot_misdaadcijfer_2(jaarcijfers):
    return []


assert(set(groot_misdaadcijfer_2(misdaadcijfer)) ==
       set(['Oost-Nederland', 'Zebra', 'Noord-Holland', 'Rotterdam', 'Limburg', 'Landelijke eenheid']))