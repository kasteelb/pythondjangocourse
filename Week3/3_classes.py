# Classes zijn erg handige dingen.
# Ze helpen om je code netjes te houden, herbruikbaar te maken, en modulair op te bouwen.
# Bovendien maakt het Django Framework veel gebruik van classes. Belangrijk om daar dus wat mee te oefenen!

# Todo: 1
# De functies 'groter_dan_tien' en 'in_juiste_range' die je hebt gemaakt in 1_comparison blijken super handig.
# De baas heeft je gevraagd om ze te bundelen in een class.
# In deze class zit al een variabele: de misdaadcijfers.
# De twee functies die je in deze functie maakt, moeten werken op de misdaadcijfers.

class AnalyseTool:
    def __init__(self, misdaadcijfers):
        self.misdaadcijfers = misdaadcijfers

    def groter_dan_tien(self):
        return []

    def in_juiste_range(self):
        return []


analyse_2019 = AnalyseTool([7, 3, 9, 34, 3, 121, 18, 1, 39, 66, 8])
assert(analyse_2019.groter_dan_tien() == [34, 121, 18, 39, 66])
assert(analyse_2019.in_juiste_range() == [34, 18, 39])


# Todo 2
# De eenheid Amsterdam wil andere waarden gebruiken bij 'in_juiste_range'.
# In plaats van >10 en <45, zijn we nu geinteresseerd in de cijfers als ze >10 zijn, en <30.
# Schrijf een tweede klasse, die zo veel mogelijk gebruik maakt (hint: inheritance) van de bovenstaande,
# die aan de wensen van Amsterdam voldoet

class AnalyseToolAmsterdam:
    pass


analyse_amsterdam = AnalyseToolAmsterdam([7, 3, 9, 34, 3, 121, 18, 1, 39, 66, 8])
assert(analyse_amsterdam.in_juiste_range() == [18])
# Todo: vraag: is de functie groter_dan_tien ook aanwezig in deze klasse? Hoe kun je dat checken?


# Todo 3
# Het probleem van Amsterdam zorgde voor extra code.
# Wat we ook hadden kunnen doen, is twee parameters aan de functie 'AnalyseTool.in_juiste_range' toevoegen.
# Probeer dat te doen in de volgende classe:

class AnalyseToolAlleEenheden:
    jaar = 2020

    def __init__(self, misdaadcijfers):
        self.misdaadcijfers = misdaadcijfers

    def groter_dan_tien(self):
        return []

    def in_juiste_range(self, ondergrens, bovengrens):
        return []


analyse_amsterdam = AnalyseToolAlleEenheden([7, 3, 9, 34, 3, 121, 18, 1, 39, 66, 8])
analyse_anderen = AnalyseToolAlleEenheden([7, 3, 9, 34, 3, 121, 18, 1, 39, 66, 8])

# Todo 4: schrijf zelf wat testen (m.b.v. assert) die checken of
# de de functies in analyse_amsterdam en analyse_anderen de juiste cijfers opleveren.
assert(1 == 1)  # <<-- aanpassen en wat meer testen toevoegen!!!


# Todo 5: errors (uitdaging?!) (google)
# Er is een hoop herrie in huis met de kinderen, en je merkt dat je vaak foutjes begint te maken.
# Dat levert verkeerde cijfers op!
# Pas bovenstaande AnalyseToolAlleEenheden aan, zodat:
# 1. Je er alleen instanties van kunt aanmaken als de lengte van de input precies 11 is (het aantal eenheden).
#    Bij meer of minder moet er een 'ValueError' worden gegooid.
# 2. De functie 'in_juiste_range' moet een 'warning' aan je teruggeven
#    als de ondergrens groter is dan, of gelijk is aan, de bovengrens.

