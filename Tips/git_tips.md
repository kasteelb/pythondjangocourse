### Git tips

- Met `git status` zie je op welke branch je nu bent.
- Met `git status` zie je ook welke bestanden je gewijzigd hebt.
- Soms zijn er nieuwe branches online aangemaakt, die je dan lokaal nog niet hebt. Met `git fetch` weet je weer welke branches er allemaal zijn.

- Soms zijn er branches geupdate door iemand anders. Dus nieuwe code erin, die je lokaal nog niet hebt. Dan kun je met `git pull` de nieuwe code krijgen. Je moet dan wel op die branch zitten.
- Andersom: als je code lokaal hebt en je wilt zorgen dat die online ook komt te staan, dan moet je daarvoor het commando `git push` gebruiken.
- Soms wil je pushen, maar is online jouw nieuwe branch er nog niet. Dan moet je het commando: `git push --set-upstream origin [naam]` gebruiken (vervang [naam] door je branchnaam en verwijder de haken).

- Als je branches wilt mergen, dan gebruik je het commando `git merge [branch-naam]`. Wat er dan gebeurt is dat de andere branch hier in wordt gemerged.