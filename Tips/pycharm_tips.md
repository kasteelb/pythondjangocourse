### Pycharm tips van de week

#### Terminal
Onder in je scherm zie je 'terminal', als je die opent dan opent zich een terminal die hetzelfde te gebruiken is als terminals buiten PyCharm.

#### Python Console
Als je in een normale terminal *python* typt, dan kom je in een omgeving terecht waar python commando's bekend zijn.
Zo'n omgeving heeft Pycharm al voor je klaargezet: de Python Console.

Hij is nog net iets vetter, omdat je ook kunt browsen door de variabelen die bekend zijn.

Hoe krijg je nu code in de python console terecht?
1. De eerste optie is om een stukje code te selecteren, rechtermuisknop, en te klikken op 'execute selection in python console'

2. Je kunt ook een hele file tegelijk runnen, door niets te selecteren en rechtermuisknop: run file in Python Console
   (er opent zich dan wel een *nieuwe* console).
   
3. Een derde optie is om code te importeren in de python console. Je doet dat door statements als: <br>
`from for_loops import *`  <- uit for_loops.py importeer alles <br>
   `from for_loops import mumbling, lost_without_a_map` <- importeer specifieke functies
   
Als optie drie niet werkt, dan zou dat kunnen liggen aan bepaalde instellingen. Je kunt zelf even kijken bij preferences -- python console. De working directory moet bv. staan op de map (directory) waar je in werkt).

#### Todo's
Je kunt een todo aanmaken met een comment: `# Todo: blablbal`. Onderin Pycharm is een tabje met TODO, daar zie je een overzicht. Het is vaak goed om die Todo's snel op te lossen, want ze zijn signalen dat je code nog niet goed (of niet af) is.

#### Opmaak
Soms heb je een lelijke file van het internet gekopieerd. Zie bv in dit project (large_json.json). Je kunt dan boven bij code --- reformat file de file wat leesbaarder maken.

Op diezelfde plek staan nog wat meer commando's, om bijvoorbeeld ongebruikte import weg te gooien, daar komt later denk ik ook nog wel van pas.

Bij python is de juiste indenting (tabs) ook van belang. Je kunt makkelijk een stuk code verschuiven door de regels te selecteren en dan op tab (vooruit) of shift-tab (terug) te drukken.

Soms wil je code van een bepaald extern pakket gebruiken, bijvoorbeeld de functie `load` uit het pakket `json`. Je typt dan waar je het nodig hebt `json.load(blablla)`. Als je dubbelklikt op json, en dan alt+enter gebruikt, dan wordt json toegevoegd aan je imports bovenaan in de files.

Soms wil je ook een stuk code bekijken dat je hebt geimporteerd o.i.d. Dat doe je door op de functie te dubbelklikken, en ctrl+b te gebruiken.

#### Debuggen met Pycharm
Dat kan ook, maar behandelen we pas als het relevant wordt.